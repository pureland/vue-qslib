import BaseQuestionSubject from './TQuestionSubject';
import RadioSelOption from './RadioSelOption';

export default class RadioSelSubject extends BaseQuestionSubject<RadioSelOption> {
    // 标题是否可以填空
    public TitleBlank: boolean = false;
    public BlankValue:number=0;
    
}
